class Node: #This class represents one "Node" or `num` in the tree
    def __init__(self, name, parent=None): #This is called when we create a node, self is passed in Python
        self.name = name #Python classes are bad, and this is the name of the node
        self.parent = parent #If the node has a parent this will not be None
        self.children = [] #This is a list of the children to this node
        # ...

        if parent: #If I have a parent
            self.parent.children.append(self) #Add this node to the list of parents children

a = [1, 2, 3, 7, 9] #List of numbers to use

### If you want to output to a text file(on linux)
### Set n to a constant and use "python treesum.py > output.txt"
n = 22
#n = int(input("Number\n-> "))

def fit(v, par): #v is the current value of n, or how far away from zero it is
    for num in a: #Simplified this loop, didn't need the index. FOR each NUMBER in a
        if v-num > 0: #If we subtracted `num` and it still isn't zero
            t = Node(str(num), par) #Create a new node to record that we subtracted `num`
            if not fit(v-num, t) and len(par.children) > 0: #This essentially checks that it's possible for 
            #this branch to ever equal zero, for example if v-num is now 1 and 1 isn't in the list of
            #numbers this will happen
                del t.children[-1] #Remove the last child from this node as it's invalid (sum would be more than n)
        elif v-num == 0: #We made it to zero!, this is the end of this branch
            t = Node(str(num), par) #Create a new node with this number
            return True #Return true to say that the branch successfully reached zero
        else: #This gets run if v-num is less than zero, the branch is NOT valid
            return False #Return false to remove this branch from the tree
    return True #After going through all the numbers without reaching zero, return true to say that this branch is fine

root = Node("Top") #Creates a root node to make the whole tree

fit(n, root) #Call fit with the number we want and the root node

def print_tree(current_node, indent="", last='updown'): #I honestly have no idea how this function works
    #But it makes a pretty tree so it's fine
    nb_children = lambda node: sum(nb_children(child) for child in node.children) + 1
    size_branch = {child: nb_children(child) for child in current_node.children}

    """ Creation of balanced lists for "up" branch and "down" branch. """
    up = sorted(current_node.children, key=lambda node: nb_children(node))
    down = []
    while up and sum(size_branch[node] for node in down) < sum(size_branch[node] for node in up):
        down.append(up.pop())

    """ Printing of "up" branch. """
    for child in up:     
        next_last = 'up' if up.index(child) is 0 else ''
        next_indent = '{0}{1}{2}'.format(indent, ' ' if 'up' in last else '│', " " * len(current_node.name))
        print_tree(child, indent=next_indent, last=next_last)

    """ Printing of current node. """
    if last == 'up': start_shape = '┌'
    elif last == 'down': start_shape = '└'
    elif last == 'updown': start_shape = ' '
    else: start_shape = '├'

    if up: end_shape = '┤'
    elif down: end_shape = '┐'
    else: end_shape = ''

    print('{0}{1}{2}{3}'.format(indent, start_shape, current_node.name, end_shape))

    """ Printing of "down" branch. """
    for child in down:
        next_last = 'down' if down.index(child) is len(down) - 1 else ''
        next_indent = '{0}{1}{2}'.format(indent, ' ' if 'down' in last else '│', " " * len(current_node.name))
        print_tree(child, indent=next_indent, last=next_last)

print_tree(root) #Print the tree so we can actually go through it